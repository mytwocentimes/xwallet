//
//  Double-ExtensionTests.swift
//  XWalletTests
//
//  Created by loj on 24.06.18.
//

import XCTest

class Double_ExtensionTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    

    public func test_toXMR() {
        let expectedXmr: UInt64 = 22080000000000

        let result = 22.08.toXMR()

        XCTAssertEqual(expectedXmr, result)
    }
    
}

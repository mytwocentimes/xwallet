//
//  UIImage-Extension.swift
//  XWallet
//
//  Created by loj on 31.08.18.
//

import Foundation
import UIKit


extension UIImage {

    func toData() -> Data? {
        if let data = UIImagePNGRepresentation(self) {
            return data
        }
        if let data = UIImageJPEGRepresentation(self, 1.0) {
            return data
        }
        return nil
    }
}

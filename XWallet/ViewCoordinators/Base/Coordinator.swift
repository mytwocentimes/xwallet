//
//  Coordinator.swift
//  XWallet
//
//  Created by loj on 22.10.17.
//

import Foundation


public protocol Coordinator: class {
    
    var childCoordinators: [Coordinator] { get set }
    
}


public extension Coordinator {
    
    public func add(childCoordinator: Coordinator) {
        if self.childCoordinators.contains(where: { $0 === childCoordinator }) {
            return
        }
        self.childCoordinators.append(childCoordinator)
    }
    
    public func remove(childCoordinator: Coordinator) {
        self.childCoordinators = self.childCoordinators.filter { $0 !== childCoordinator }
    }
    
}
